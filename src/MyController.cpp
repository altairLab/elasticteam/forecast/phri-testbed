#include <MyController.hpp>

using namespace forecast ; // or whatever you are using

MyController::MyController ()
    : Controller (1) { // notice the 1, i.e. the number of parameters
    // nothing to do here
}

MyController::MyController(float kp)
    : Controller (1) , // same as above
    kp(kp) // initialize the ’ kp ’ member , copy by value
{
    Controller :: initialized = true ; // set the initialized member to true
}

bool MyController::init(const std::vector<float> &params)
{
    if (params.size() != numberOfParams)
        return false;
    kp = params[0];
    return initialized = true; // set the initialized member to true
}

float MyController::process(const IHardware *hw,
                                float ref,
                                float dref,
                                float ddref)
{
    /*
        Common methods for accessing HW internal values

        hw->getT() time
        hw->getThetaM() current angle (rad)
        hw->getTauM() torque exerted by the motor
        hw->getTauS() torque exerted by the environment (load cell sensor)
    */
    theta = hw->getThetaM();
    err = ref - theta;
    float output = kp*err;
    return output;
}

std::vector<std::string> MyController::getParamNames() const
{
    return { "KP" }; // return a string vector , in this case
                    // of just one element
}
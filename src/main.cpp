#ifdef TARGET_NUCLEO_L432KC

#include <mbed.h>
#include <forecast/platforms/RPC/RPCApp.hpp>

#include <MyController.hpp>

int main() {

    forecast::RPCApp app;

    app.setLogger([](float motorRef, const forecast::RPCHardware* hw,
                     const forecast::Controller* motor) {
        return std::vector<float>{
            hw->getThetaM(),
            hw->getTauM()
        };
    });

    // Hard-coded reference for the motor
    app.setMotorRefGen([](const forecast::RPCHardware* hw) {
        return 0.0f;
    });

    // TODO: Motor controller
    // app.setMotor(new insert_controller_here);

    // Handshake with the PC
    app.waitConnection();

    // Require parameters for the controllers which are not already initialized
    app.requireMotorParams();

    // Require the loop frequency
    auto freq = app.requireFloatValue("Loop frequency");

    // Execute control loop
    app.execControlLoop(static_cast<ulong>(freq));
}

#endif

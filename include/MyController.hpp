#ifndef MYCONTROLLER_H // In order not to define this class multiple times
#define MYCONTROLLER_H

#include <forecast/Controller.hpp>

namespace forecast {
    class MyController : public Controller {
        public: // notice the public keyword , don ’t miss it
            MyController();
            MyController(float kp);
            virtual bool init(const std::vector<float> &params) override;
            virtual std::vector<std::string> getParamNames() const override;
            virtual float process (const IHardware *hw,
                                float ref,
                                float dref = 0,
                                float ddref = 0) override;
        protected :
            float kp = 0.0;
            float theta = 0.0f;
            float err = 0.0;
    };
}
#endif